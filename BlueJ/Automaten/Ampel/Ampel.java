package Automaten.Ampel;


public class Ampel{
	private String state = null;
	public void schalteAufGruen(){
		if (state!=null&&state.equals("rot"))
			state="gruen";
	}
	public void schalteAufRot(){
		if (state!=null&&state.equals("gruen"))
			state="rot";
	}
	public void schalteAn(){
		if (state==null)
			state="rot";
	}
	public void schalteAus(){
		if (state!=null)
			state="rot";
	}
	public String getState(){
		return state;
	}
}

