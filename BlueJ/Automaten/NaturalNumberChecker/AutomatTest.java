package Automaten.NaturalNumberChecker;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test; 
import java.util.Random;

/**
 * The test class AutomatTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class AutomatTest
{
    String[] tests;
 
    int tries=10000;
    int maxLenght=50;
    /**
     * Default constructor for test class AutomatTest
     */
    public AutomatTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        tests=new String[tries];
        Random r = new Random();
        for (int i=0; i<tries; i++){
            tests[i] = "";
            switch (i%3){
                case 0: tests[i]+="+";
                        break;
                case 1: tests[i]+="-";
            }
            tests[i]+=(r.nextInt(9)+1);
            int length=  r.nextInt(maxLenght);
            for (int j=0; j<length-1; j++){
                tests[i]+=r.nextInt(10);
            }
            System.out.println(tests[i]);
        }
        
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @Test
    public void test()
    {
        for (String s:tests){
            assertTrue(Automat.check_if_valid(s));
        }
    }
}
