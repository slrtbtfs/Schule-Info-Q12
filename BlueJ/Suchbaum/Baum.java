package Suchbaum;

import java.util.Vector;

public class Baum{
    Node root;
    
    public Baum(){
   
        //root = new Node("Wurzel",b, d);
    }
    
    
    public void einfuegen(int n){
        if (root == null){
            root = new Node(n);
        }else{
            root.einfuegen(n);
        }
    
    }
    
    public Node suchen(int n){
       long before = System.nanoTime();
       Node tmp =  root.suchen(n);     
       System.out.println("Needed " + (System.nanoTime()-before) + "ns");
       return tmp;
    }
    
    public Node suche_sortiert(int n){
        Node tmp =  root.suche_sortiert(n);       
        return tmp;
    }
    
    
    public void traversieren(){
        root.traversieren();
    }
    
        
    public int zaehlen(){
        return root.zaehlen();
    }
    
      public Vector<Integer> erzeugeListe(){
          Vector<Integer>v = new Vector<Integer>();
          
          root.erzeugeListe(v);
          return v;
          
          
      }
}