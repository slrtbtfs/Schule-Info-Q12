package Suchbaum;

import java.util.Vector;

public class Node{
    int d;

    Node Left;
    Node Right;

    public Node(int n){
        d = n;
    }

    public Node suchen(int n){
        if (n==this.d){
            return this;
        }else{
            Node vonLinks=null;
            if (this.Left != null){
                vonLinks = this.Left.suchen(n);           
            }

            if (vonLinks != null) return vonLinks;

            if (this.Right != null ){
                return this.Right.suchen(n);
            }else{
                return null;
            }

        }
    }    

    public void einfuegen(int n){
        if (this.d < n){
            if (Right != null){
                Right.einfuegen(n);
            }else{
                Right = new Node(n);
            }

        }else{
            if (Left != null){
                Left.einfuegen(n);
            }else{
                Left = new Node(n);
            }
        }

    } 

    public Node suche_sortiert(int n){
        if (this.d == n){
            return this;
        }else if (n > this.d && this.Right != null){
            return Right.suche_sortiert(n);                        
        }else if (n < this.d && this.Left != null){
            return Left.suche_sortiert(n);
        }else{
            return null;
        }
    }

    public void traversieren(){

        if (this.Left != null){
            Left.traversieren();
        }
        System.out.print(this.d + " ");
        if (this.Right != null){
            Right.traversieren();
        }

    }

    public int zaehlen(){
        int r=1;

        if (this.Left != null){
//            r= r+ Left.zaehlen();
            r+=Left.zaehlen();
        }
        //System.out.print(this.d + " ");
        if (this.Right != null){
            r+=Right.zaehlen();
        }

        return r;      
    }
       
    public void erzeugeListe(Vector<Integer> Liste){
        if (this.Left != null){
            Left.erzeugeListe(Liste);
        }
        Liste.add(new Integer(this.d));
        
        if (this.Right != null){
            Right.erzeugeListe(Liste);
        }
    }
}