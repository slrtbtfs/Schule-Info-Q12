import javakara.JavaKaraProgram;
        
/* BEFEHLE:  kara.
 *   move()  turnRight()  turnLeft()
 *   putLeaf()  removeLeaf()
 *
 * SENSOREN: kara.
 *   treeFront()  treeLeft()  treeRight()
 *   mushroomFront()  onLeaf()
 */
public class mandelbrot extends JavaKaraProgram {
    private boolean check(int X, int Y){
        float a = (X-100)/500;
        float b = (Y-100)/500;
        
        float x=a;
        float y =b;
        float x_new;
        float y_new;
        /*for (int i = 0; i<10; i++){
            x_new = x*x - y*y + a;
            y_new = 2*x*y + b;
            x=x_new;
            y=y_new;
        }*/
        return ((x*x + y*y) <= 4);
    }
  // hier k�nnen Sie eigene Methoden definieren

  public void myProgram() {
    // hier kommt das Hauptprogramm hin, zB:
    for (int y = 0; y<200;y++){
        for (int x = 0; x<200;x++){
            kara.move();
            if(check(x,y)) {
                kara.putLeaf();
                System.out.println("Should put leaf at " + x
                + " " + y);
            }
    }
    //one row up
    kara.turnLeft();
    kara.move();
    kara.turnRight();
  }
}
}

        