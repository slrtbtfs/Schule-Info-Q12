# Kryptographie
## Caesar Chiffre
Bereits bekannt: Caesar-Chiffre (Im Alphabet um n Stellen verschieben)

Klartext, Schluessel -> Chiffre

        ---------------------  --------------
        | Klartext(Chiffre) |  | Schluessel |
        ---------------------  --------------
             |                  |
             |-------------------
             V
         /-----------------\
        ( (de)chiffrieren   ) 
         \-----------------/
            |
            V
        ----------------------
        | Chiffre (Klartext) |
        ----------------------


### Code
        String s = "Wochenende!";
        "toCharArray"
        a[0] = 'W';
        
        => Char to ASCII, dann verschieben
        => Verschobener Code in Zeichen umwandeln
        => Zu Chiffre zusammenfuegen

       
### Problem
Durch begrenzte Anzahl der Schluessel extrem schnell zu knacken.


## Vigenere-Chiffre
Bsp:
Geg: Matrix mit 26x26 chars

        KLARTEXT
        PASSPASS
        ZLSJIEPL

chiffrieren:

        K => K.te Spalte \
                          > Chiffre: Z
        P => P.te Zeile  /

dechiffrieren:
        Chiffre:        J
        Schluesselteil: P
        
        1. Suche in Zeile P das J
        2. Klartext ist der Buchstabe in der 1. Zeile derselben Spalte

## Kacken
* Ausprobieren aller Schlüssel 
 - Auf Woerter aus Woerterbuch pruefen
 - Haeufigkeitsanalyse der Buchstaben sprachspezifisch
* Haeufigkeitsanalyse d. Buchstaben
 - ggf. Problem mit der Schluessellaenge bei Vigenere
 
## Klassisches Problem
Man-in-the-middle

        Alice           Mallory         Bob
           |               |             |
           |    Key        |             |
           |---------------+------------>|
           |               |             |
           |               |             |
           |               |             |
           |               |             |
           |               |             |
           |               |             |
           |               |             |
           |               |             |
           |               |             |
           |               |             |


        -----------  --------------
        | Chiffre |  | Schluessel |-------------+
        -----------  --------------             |  
             |                  |               |
             |-------------------               |
             V                                  |
         /-----------------\                    |
        ( dechiffrieren     )                   |
         \-----------------/                    |
            |                                   |
            V                                   |
        -----------                             |
        | Klartext|                             |
        -----------                             |
                                                |
                                                +------> Schluessel identisch 
                                                |           => symmetrische Verschluesselung
                                                |       falls unterschiedliche Schluessel 
        -----------  --------------             |           => asymetrische Verschluesselung
        | Klartext|  | Schluessel |-------------+
        -----------  --------------
             |                  |
             |-------------------
             V
         /-----------------\
        ( chiffrieren      ) 
         \-----------------/
            |
            V
        ----------
        | Chiffre|
        ----------

# Diffie Hellman

![Schluesseltausch](https://upload.wikimedia.org/wikipedia/commons/4/4c/Public_key_shared_secret.svg)
![Farbmischung](https://upload.wikimedia.org/wikipedia/commons/9/9d/Diffie-Hellman_Key_Exchange_\(de\).svg)

# Asymetrische Verschluesselung
Verschluesseln mit einem Schluessel. Entschluesseln mit einem anderen Schluessel.

        -----------  --------------
        | Klartext|  | Schluessel |
        -----------  --------------
             |                  |
             |-------------------
             V
         /-----------------\
        ( Encrypt           ) 
         \-----------------/
            |
            V
        ----------
        | Chiffre|
        ----------

Ideen:
* Verschluesseln ueber quadrieren/potenzieren
 - Schluessel: Exponent
 - __Symmetrisch, da Schluessel identisch__
 - Entschluesseln durch Wurzelziehen / n-te Wurzel
 - Problem: ASCII 8 Bit (0-255) A:65 => quadriert -> 255
 - Abbildung zurueck in gueltigen Bereich: modulo
Problematisch: sicherstellen der Ein-eindeutigkeit / Bijektivitaet / Umkehrbarkeit

## RSA
1\. Schluesselpaar e, N
2\. Schluesselpaar d, N

e,d,N so gewaehlt, dass gilt:
          m^e mod N = c fuer alle Nachrichten m
        ^ c^d mod N = m fuer alle Nachrichten m

__Der Zusammenhang zw. e, d, N ist nicht offensichlich__
ueblich: Ein Schluesselpaar oeffentlich.
Sinn: __A__ verschluesselt Text mit Hilfe des public Key von __B__
Nachricht kann nur mit geheimen Schluessel (_private key_) von __B__ entschluesselt werden.
_=> geheime Nachricht an B_
alternativer Zweck:
__A__ verschluesselt mit ihrem private key => Jeder kann mit A's public key entschluesseln. ^= Signatur
