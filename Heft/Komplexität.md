# Berechenbarkeit & Komplexit�t
## Komplexit�t
### 1. Laufzeitkomplexit�t (Geschwindigkeit)
#### Messen der Laufzeit
        t1 = Zeit direkt davor
        Algorithmus/Programm
        t2 = Zeit direkt danach
        Laufzeit = t2 - t1
* + einfach
* - Programm �ndern!
* - Systemabh�ngig => Ergebnisse nur auf gleichem System vergleichbar!
* - viele Prozesse "gleichzeitig"
        - viele Programme gleichzeitig
        - *langsamer!*
* JIT (Just in Time complier) => Java optimiert w�hrend der Ausf�hrung!

#### Z�hlen der Rechenschritte
* Analyse des Quellcodes
* Wichtig: Gr��e der Eingabe relevant, �blicherwei�e variabel
Bsp: Verkettete Liste
                        
        0                                             /
        |     1x pr�fen ob next == null              /
        | +3  1x Addieren + 1x rekursiver Aufruf     \        (n-1)x
        V                                             \
        1
        |
        | +3
        V
        2
        |
        | +3
        V
        3
        |
        | +1  1x rek.
        V
        n
=> Laufzeit in Abh�ngigkeit von n: 

        3 * (n-1)       +       1       +      n      = 4n - 2 \in O(n)
        3 Zeilen bei Rek        letzte Pr      return


Problem: nicht jede OP braucht gleich viel Zeit
Anweisungenz�hlen bei Gr��e der Eingabe: n <-- L�nge eines Feldes/Anzahl/Knoten/ obere Grenze f�r Rechenweg / ...

Bsp:
        25n^2 - 32n    \in O(n^2)
        2^n   + 35n^37 \in O(2^n)

#### Bubble Sort:
        for x in range (n)
                for y in range (n)              \
                        if (...)                 > min n Ops. (Vergleiche)
                                swap(...)       /
Problem bei exakter Z�hlung -> nie bekannt, ob Tausch notwendig, *aber* Vergleich immer
* innere Schleife \in O(n)
* �u�ere Schleife \in O(n^2)

#### Max. aus Liste
        for i in Liste
                if i > max
                        max 0 = i



         rek:
        get (root)
        while true
                if i>max
                        max = i
                if (next == 0)
                        ret max
                get(next)

* Jeweils n Wiederholungen
* O(n) 


### 2. Speicherkomplexit�t 
