# Berechenbarkeit
Sind Werte berechenbar?
* theoretisch?
* praktisch?

Praktisch: 
* pi nicht berechenbar (unendliche Laufzeit!)
__aber__ theoretisch korrekter Algorithmus
* Zweiter Schlüssel für RSA: berechenbar, aber viel zu lange Laufzeit (brute force)

* Das __"Halte-Problem"__
 - Algorithmus A erhält Eingabe n
 - Algorithmus B soll eintscheiden, ob A mit n terminiert

Problem: B kann nicht entscheiden, ob A jemals terminiert!
 * A könnte sehr lange brauchen
 * A könnte in einer Endlosschleife stecken

__Bsp:__
* Vergleich zweier reeler Zahlen
 - __Q__ (Brüche): berechenbar
 - __irrat. Zahlen__ unendlich, unperiodisch: bei a?=b: Ziffern vergleichen => Stop, wenn Ziffer aus a != Ziffer aus b. => nicht entscheidbar, ob Algorithmus jemals terminiert.

* Die Ackermann-fkt.

        func A(n int, m int)int{
                if n==0{
                        return m+1
                } else if m==0{
                        return A(n-1, 1)
                } else {
                        return A(n-1, A(n, m-1)
                }
        }


        A (n,m):
         |
        0|
        1|
        2|
                        
