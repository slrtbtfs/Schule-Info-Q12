# Organisation beim Ablauf eines Asm-Programms

am Bsp. Minimaschine


        
           ---------------Datenbus-------------------------------
           |                                                    |
                                                        Speicher:
        Akku:                   Befehlsregister:        0:      265
                                                        1:      30
                                                        2:      269
                                                        3:      31
                                Progammz:               ...
        Status:
        Z: N: V:

           |                                                    |
           ---------------Adressbus------------------------------

* CPU: Central Processing Unit
* Datenbus: scheibt/liest im Speicher
* Adressbus: Regelt, welche Speicherstelle im Zugriff genutzt wird
* Befehl im Register regelt, ob gelesen oder geschrieben wird
## Lernbegriffe
* Datenbus: Bidirektional
* Addressbus: Unidirektional
* Minischritte -> S.104
## Overflow Flag (V)
16 Bit: 0 bis 2^16 - 1 = 65535

